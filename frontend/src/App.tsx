import React, { Suspense } from 'react'
import './App.css'
import { Board } from './components/Board/Board'

function App() {
  return (
    <div className="App">
      <Suspense fallback="...loading">
        <Board />
      </Suspense>
    </div>
  )
}

export default App
