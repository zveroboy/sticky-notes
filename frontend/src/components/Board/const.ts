export const NOTES_STORAGE_KEY = 'NOTES'
export const NOTE_PARENT_CLASS = 'noteItem'
export const NOTE_DRAG_CLASS = 'noteItemDrag'
export const THROTTLE_TIMEOUT_MS = 500
export const NEW_NOTE_DEFAULT_OFFSET = 10
