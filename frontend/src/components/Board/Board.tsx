import React, {
  FC,
  MouseEvent,
  useCallback,
  useEffect,
  useReducer,
  useRef,
  useState,
} from 'react'
import { StickyNote } from '../StickyNote/StickyNote'
import { getNotes, loadNotes, Note } from '../../models/Note'

import { NOTE_DRAG_CLASS, NOTE_PARENT_CLASS } from './const'
import { ActionTypes, BoardState, createBoardState, reducer } from './reducer'
import { suspend } from '../../utils/suspend'
import './Board.css'

const suspendedNotes = suspend(loadNotes())

export const Board: FC = () => {
  useState(() => suspendedNotes.read())
  const deltaRef = useRef({
    x: 0,
    y: 0,
  })
  const rafRef = useRef<number>()
  const dragStartRef = useRef({
    uid: '',
    offsetX: 0,
    offsetY: 0,
    width: 0,
    height: 0,
    trashX: 9999,
    trashY: 9999,
  })

  const [state, dispatch] = useReducer(
    reducer,
    createBoardState(),
    (initData) => ({
      ...initData,
      notes: getNotes(),
    }),
  )

  const changeHandler = useCallback(
    (uid: Note['uid'], patch: Partial<Note>) =>
      dispatch({
        type: ActionTypes.CHANGE_NOTE,
        uid,
        patch,
      }),
    [],
  )

  const mouseDownHandler = (event: MouseEvent) => {
    const target = event.target as HTMLElement
    const noteParentElement = target.closest<HTMLElement>(
      `.${NOTE_PARENT_CLASS}`,
    )

    if (noteParentElement) {
      const { noteUid: uid } = noteParentElement.dataset as { noteUid: string }
      dispatch({ type: ActionTypes.PUT_NOTE_IN_FRONT, uid })
      if (target.classList.contains(NOTE_DRAG_CLASS) && trashRef.current) {
        const trashRect = trashRef.current.getBoundingClientRect()
        dispatch({ type: ActionTypes.CHANGE_DRAGGING, dragging: true })
        const rect = noteParentElement.getBoundingClientRect()
        dragStartRef.current = {
          uid,
          offsetX: event.clientX - rect.left,
          offsetY: event.clientY - rect.top,
          width: rect.width,
          height: rect.height,
          trashX: trashRect.left,
          trashY: trashRect.top,
        }
      }
    }
  }

  const mouseUpHandler = () => {
    if (rafRef.current) {
      cancelAnimationFrame(rafRef.current)
      rafRef.current = undefined
    }
    dispatch({ type: ActionTypes.CHANGE_DRAGGING, dragging: false })
    dispatch({ type: ActionTypes.TRASH_ACTIVITY, active: false })
    if (
      state.dragging &&
      state.trash &&
      dragStartRef.current.uid &&
      window.confirm('Delete this note?')
    ) {
      dispatch({ type: ActionTypes.DELETE_NOTE, uid: dragStartRef.current.uid })
    }
  }

  const moveHandlerRaf = () => {
    if (!state.dragging) {
      return
    }

    changeHandler(dragStartRef.current.uid, {
      left: deltaRef.current.x,
      top: deltaRef.current.y,
    })

    if (
      deltaRef.current.x + dragStartRef.current.width >
        dragStartRef.current.trashX &&
      deltaRef.current.y + dragStartRef.current.height >
        dragStartRef.current.trashY
    ) {
      if (!state.trash) {
        dispatch({ type: ActionTypes.TRASH_ACTIVITY, active: true })
      }
    } else if (state.trash) {
      dispatch({ type: ActionTypes.TRASH_ACTIVITY, active: false })
    }

    rafRef.current = undefined
  }

  const mouseMoveHandler = (event: MouseEvent) => {
    if (rafRef.current) {
      return
    }

    rafRef.current = requestAnimationFrame(moveHandlerRaf)
    deltaRef.current = {
      x: event.clientX - dragStartRef.current.offsetX,
      y: event.clientY - dragStartRef.current.offsetY,
    }
  }
  const trashRef = useRef<HTMLDivElement>(null)

  const stateRef = useRef<BoardState>(state)

  useEffect(() => {
    stateRef.current = state
  }, [state])

  const resizeObserverHandler = (entries: ResizeObserverEntry[]) => {
    for (let {
      contentRect: { width, height },
      target,
    } of entries) {
      const { noteUid } = (target as HTMLElement).dataset as {
        noteUid: string
      }
      const note = stateRef.current.notes[noteUid]
      if (note && (width !== note.width || height !== note.height)) {
        changeHandler(note.uid, { width, height })
      }
    }
  }

  const resizeObserverRef = useRef<ResizeObserver>(
    new ResizeObserver(resizeObserverHandler),
  )

  useEffect(() => {
    const resizeObserver = resizeObserverRef.current
    return () => {
      resizeObserver.disconnect()
    }
  }, [])

  return (
    <div
      className="Board"
      onMouseDown={mouseDownHandler}
      onMouseMove={state.dragging ? mouseMoveHandler : undefined}
      onMouseUp={state.dragging ? mouseUpHandler : undefined}
    >
      <div
        className={`Board__trash${state.trash ? ' Board__trash--active' : ''}`}
        ref={trashRef}
      >
        <span>🗑️</span>
      </div>
      {Object.values(state.notes).map((note) => (
        <StickyNote
          resizeObserver={resizeObserverRef.current}
          note={note}
          onChange={changeHandler}
          clickDetectClass={NOTE_PARENT_CLASS}
          dragDetectClass={NOTE_DRAG_CLASS}
          key={note.uid}
        />
      ))}
      <div className="Board__actions">
        <button
          onClick={() =>
            dispatch({
              type: ActionTypes.ADD_NOTE,
            })
          }
        >
          Add note
        </button>
      </div>
    </div>
  )
}
