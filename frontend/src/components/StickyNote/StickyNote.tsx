import React, { FC, memo, useEffect, useRef, useState } from 'react'
import { Note } from '../../models/Note'
import { HUE_COLOR_MAX } from '../../utils/const'
import './StickyNote.css'

const dragStartHandler = () => false

interface INoteProps {
  resizeObserver: ResizeObserver
  note: Note
  clickDetectClass: string
  dragDetectClass: string
  onChange: (uid: Note['uid'], patch: Partial<Note>) => void
}

export const StickyNote: FC<INoteProps> = memo(
  ({ note, clickDetectClass, resizeObserver, dragDetectClass, onChange }) => {
    const [size] = useState([note.width, note.height])
    const noteRef = useRef<HTMLDivElement>(null)

    useEffect(() => {
      const noteElement = noteRef.current
      noteElement && resizeObserver.observe(noteElement)
      return () => {
        noteElement && resizeObserver.unobserve(noteElement)
      }
    }, [resizeObserver])

    const [isColorRangeVisible, setColorRangeVisible] = useState(false)

    return (
      <div
        ref={noteRef}
        onDragStart={dragStartHandler}
        className={`StickyNote ${clickDetectClass}`}
        data-note-uid={note.uid}
        style={
          {
            top: note.top + 'px',
            left: note.left + 'px',
            width: size[0] + 'px',
            height: size[1] + 'px',
            zIndex: note.depth,
            '--bg-color': note.hue,
          } as React.CSSProperties
        }
      >
        <header className={`StickyNote__header ${dragDetectClass}`} />
        <div className="StickyNote__body">
          <textarea
            className="StickyNote__contentEditor"
            onChange={(e) =>
              onChange(note.uid, {
                content: e.target.value,
              })
            }
            defaultValue={note.content}
          />
        </div>
        <footer className="StickyNote__footer">
          <span
            className="StickyNote__colorRangeToggle"
            onClick={() => setColorRangeVisible(!isColorRangeVisible)}
          >
            🎨
          </span>
          <div hidden={!isColorRangeVisible}>
            <input
              type="range"
              min={0}
              max={HUE_COLOR_MAX - 1}
              value={note.hue}
              onChange={(e) =>
                onChange(note.uid, {
                  hue: +e.target.value,
                })
              }
            />
          </div>
        </footer>
      </div>
    )
  },
)
