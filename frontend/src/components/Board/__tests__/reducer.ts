import { ActionTypes, createBoardState, reducer } from '../reducer'
import { genUid } from '../../../utils/utils'
import { createNote } from '../../../models/Note'
import { NOTES_STORAGE_KEY } from '../const'

jest.mock('../../../utils/utils')
const genUidMock = genUid as jest.Mock

const setItemMock = jest.fn()
Storage.prototype.setItem = setItemMock

describe('Board reducer', () => {
  it('should add', () => {
    const newNoteUid = 'new-uid'
    genUidMock.mockImplementation(() => newNoteUid)
    const state = reducer(createBoardState(), { type: ActionTypes.ADD_NOTE })
    expect(state.notes[newNoteUid]).toBeDefined()
    expect(state.notes[newNoteUid].content).toEqual('New note')
    expect(setItemMock).toBeCalledWith(
      NOTES_STORAGE_KEY,
      JSON.stringify(state.notes),
    )
  })
  it('should delete', () => {
    const initState = createBoardState()
    const newNoteUid = 'new-uid'
    const note = createNote({ uid: newNoteUid })
    initState.notes[note.uid] = note
    expect(initState.notes[newNoteUid]).toBeDefined()
    const state = reducer(initState, {
      type: ActionTypes.DELETE_NOTE,
      uid: newNoteUid,
    })
    expect(state.notes[newNoteUid]).not.toBeDefined()
    expect(setItemMock).toBeCalledWith(NOTES_STORAGE_KEY, '{}')
  })
  it('should put note in front', () => {
    const initState = createBoardState()
    const noteA = createNote({ uid: 'note-uid-a', depth: 0 })
    const noteB = createNote({ uid: 'note-uid-b', depth: 1 })
    const noteC = createNote({ uid: 'note-uid-c', depth: 2 })
    initState.notes = {
      [noteA.uid]: noteA,
      [noteB.uid]: noteB,
      [noteC.uid]: noteC,
    }
    const state = reducer(initState, {
      type: ActionTypes.PUT_NOTE_IN_FRONT,
      uid: noteA.uid,
    })
    expect(state.notes[noteA.uid].depth).toEqual(3)
  })
  it('should change once', () => {
    const initState = createBoardState()
    const newNoteUid = 'new-uid'
    const note = createNote({ uid: newNoteUid })
    initState.notes[note.uid] = note
    expect(initState.notes[newNoteUid]).toBeDefined()
    reducer(initState, {
      type: ActionTypes.CHANGE_NOTE,
      uid: note.uid,
      patch: {
        content: 'content 1',
      },
    })
    reducer(initState, {
      type: ActionTypes.CHANGE_NOTE,
      uid: note.uid,
      patch: {
        content: 'content 2',
      },
    })
    const state = reducer(initState, {
      type: ActionTypes.CHANGE_NOTE,
      uid: note.uid,
      patch: {
        content: 'content 3',
      },
    })
    expect(state.notes[newNoteUid].content).toEqual('content 3')
    expect(setItemMock).toHaveBeenCalledTimes(1)
  })
})
