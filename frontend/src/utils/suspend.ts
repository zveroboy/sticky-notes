export const suspend = <T>(promise: Promise<T>): Suspended<T> => {
  let result: T
  let status = 'pending'
  const suspender = promise.then(
    (response) => {
      status = 'success'
      result = response
    },
    (error) => {
      status = 'error'
      result = error
    },
  )

  return {
    read: () => {
      switch (status) {
        case 'pending':
          throw suspender
        case 'error':
          throw result
        default:
          return result
      }
    },
  }
}

export type Suspended<T> = {
  read: () => T
}
