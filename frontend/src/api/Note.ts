import { Note } from '../models/Note'

export const fetchNotes = (): Promise<Record<string, Note>> =>
  new Promise((resolve) => {
    setTimeout(
      () =>
        resolve({
          'test-a': {
            uid: 'test-a',
            top: 20,
            left: 50,
            width: 300,
            height: 300,
            hue: 123,
            depth: 0,
            content: 'This is first note',
          },
          'test-b': {
            uid: 'test-b',
            top: 40,
            left: 250,
            width: 250,
            height: 250,
            hue: 234,
            depth: 1,
            content: 'Hello from second note',
          },
        }),
      2000,
    )
  })

export const postNotes = (_: Record<string, Note>): Promise<void> =>
  new Promise((resolve) => {
    setTimeout(() => resolve(), 300)
  })
