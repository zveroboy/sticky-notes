import { throttle } from 'underscore'
import { createNote, Note, saveNotes } from '../../models/Note'
import { genUid } from '../../utils/utils'
import { Reducer } from '../../utils/types'
import { NEW_NOTE_DEFAULT_OFFSET, THROTTLE_TIMEOUT_MS } from './const'

export type BoardState = {
  dragging: boolean
  trash: boolean
  notes: Record<string, Note>
}

export enum ActionTypes {
  ADD_NOTE = 'ADD_NOTE',
  DELETE_NOTE = 'DELETE_NOTE',
  CHANGE_NOTE = 'CHANGE_NOTE',
  PUT_NOTE_IN_FRONT = 'PUT_NOTE_IN_FRONT',
  CHANGE_DRAGGING = 'CHANGE_DRAGGING',
  SET_DRAG_START_DATA = 'SET_DRAG_START_DATA',
  TRASH_ACTIVITY = 'TRASH_ACTIVITY',
}

export interface AddNoteAction {
  type: ActionTypes.ADD_NOTE
}

export interface DeleteNoteAction {
  type: ActionTypes.DELETE_NOTE
  uid: Note['uid']
}

export interface PutNoteInFrontAction {
  type: ActionTypes.PUT_NOTE_IN_FRONT
  uid: Note['uid']
}

export interface ChangeNoteAction {
  type: ActionTypes.CHANGE_NOTE
  uid: Note['uid']
  patch: Partial<Note>
}

export interface ChangeDraggingAction {
  type: ActionTypes.CHANGE_DRAGGING
  dragging: BoardState['dragging']
}

export interface DragStartDataAction {
  type: ActionTypes.SET_DRAG_START_DATA
}

export interface TrashActivityAction {
  type: ActionTypes.TRASH_ACTIVITY
  active: BoardState['trash']
}

export type BoardActionTypes =
  | AddNoteAction
  | DeleteNoteAction
  | ChangeNoteAction
  | ChangeDraggingAction
  | PutNoteInFrontAction
  | DragStartDataAction
  | TrashActivityAction

const getMaxDepth = (notes: Note[]) => {
  let depths = notes.map((n) => n.depth)
  return depths.length ? Math.max(...depths) : 0
}

export const saveNoteThrottled = throttle(saveNotes, THROTTLE_TIMEOUT_MS)

export const reducer: Reducer<BoardState, BoardActionTypes> = (
  state: BoardState,
  action: BoardActionTypes,
): BoardState => {
  switch (action.type) {
    case ActionTypes.ADD_NOTE: {
      const notesNumber = Object.keys(state.notes).length
      const newNote = createNote({
        uid: genUid(),
        top: notesNumber * NEW_NOTE_DEFAULT_OFFSET,
        left: notesNumber * NEW_NOTE_DEFAULT_OFFSET,
        depth: getMaxDepth(Object.values(state.notes)) + 1,
      })
      let notes = { ...state.notes, [newNote.uid]: newNote }
      saveNotes(notes)
      return { ...state, notes: notes }
    }
    case ActionTypes.DELETE_NOTE: {
      delete state.notes[action.uid]
      saveNotes(state.notes)
      return { ...state }
    }
    case ActionTypes.CHANGE_NOTE: {
      const changedNote = {
        ...state.notes[action.uid],
        ...action.patch,
      }
      state.notes[changedNote.uid] = changedNote
      saveNoteThrottled(state.notes)
      return { ...state }
    }
    case ActionTypes.PUT_NOTE_IN_FRONT: {
      const note = state.notes[action.uid]
      const maxDepth = getMaxDepth(Object.values(state.notes))
      if (note.depth >= maxDepth) {
        return state
      }
      state.notes[note.uid] = {
        ...note,
        depth: maxDepth + 1,
      }
      saveNotes(state.notes)
      return { ...state }
    }
    case ActionTypes.CHANGE_DRAGGING: {
      return { ...state, dragging: action.dragging }
    }
    case ActionTypes.TRASH_ACTIVITY: {
      return { ...state, trash: action.active }
    }
    default: {
      throw new Error('Unknown action')
    }
  }
}

export const createBoardState = (): BoardState => ({
  trash: false,
  dragging: false,
  notes: {},
})
