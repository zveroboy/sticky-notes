import { HUE_COLOR_MAX } from '../utils/const'
import { NOTES_STORAGE_KEY } from '../components/Board/const'
import { fetchNotes, postNotes } from '../api/Note'
import { genUid } from '../utils/utils'

export type Note = {
  uid: string
  depth: number
  top: number
  left: number
  width: number
  height: number
  hue: number
  content: string
}

export const createNote = (patch: Partial<Note>): Note => ({
  uid: '',
  depth: 0,
  top: 0,
  left: 0,
  width: 200,
  height: 200,
  hue: Math.floor(Math.random() * HUE_COLOR_MAX),
  content: 'New note',
  ...patch,
})

export const saveNotes = (notes: Record<string, Note>) => {
  localStorage.setItem(NOTES_STORAGE_KEY, JSON.stringify(notes))
  postNotes(notes)
}

export const loadNotes = async () => {
  if (localStorage.getItem(NOTES_STORAGE_KEY)) {
    return
  }
  saveNotes(await fetchNotes())
}

export const getNotes = (): Record<string, Note> => {
  try {
    const storageNotes: Record<string, Note> = JSON.parse(
      localStorage.getItem(NOTES_STORAGE_KEY) + '',
    )
    if (storageNotes && typeof storageNotes === 'object') {
      return storageNotes
    }
  } catch (e) {}
  const newNote = createNote({ uid: genUid() })
  return { [newNote.uid]: newNote }
}
