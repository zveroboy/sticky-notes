### This project is written using
* Typescript
* React 
* Underscore
* UTF-8 Emojis

### Tests
* Jest

### CI/CD
* Bitbucket Pipelines (./bitbucket-pipelines.yml)

### Hosting
* AWS S3 ([https://nk-sticky-notes.s3.eu-north-1.amazonaws.com/index.html](https://nk-sticky-notes.s3.eu-north-1.amazonaws.com/index.html))

The Board component allows to drag and resize Sticky Notes. 
For the better performance, the requestAnimationFrame was used.
To detect Sticky Note resize the ResizeObserver was integrated.

The StickyNote component allows to edit note content and 
change note color by editing value of the color range input. 